## When creating VM instances

On GCP project, go to the Compute Engine section and create a new VM.

- All VM’s must follow the **name pattern** *nj-{name}-{type_of_machine}* e.g.: nj-Mayllon-jenkins; nj-Bruno-sonarqube.
- Pick one of the following regions: us-central1, us-west1, us-east1. Careful here, because there are limits on the nº of VM's on a given region. 
- All VM instances must have **Block project-wide SSH keys** and **Turn on Secure Boot** checked at creation time
- As **metadata**, the following value must be added when creating VM's: **enable-oslogin=True**




On Identity and API access, choose a service account that contains the following permissions:

- Compute Instance Admin (v1) 
- Kubernetes Engine Admin 
- If that service account does not exists, ask someone responsible for this project  
- Use as last resource the option: Allow full access to all cloud apis

On the configurations of the VM, enable the following parameters:

- Allow **HTTP** traffic
- Allow **HTTPS** traffic


After you create the Virtual Machine, on _Compute Engine > Virtual Machines_, access the tab Instance Schedule.
- Choose an instance schedule (8am-8pm at working days), and add your VM.
- If there's no instance schedule you can create one or talk to the responsible people.

:warning: **If any of the above rules are broken the VM will be removed!!!**

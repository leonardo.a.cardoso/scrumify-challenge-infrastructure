package pt.com.scrumify.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.services.ImapReaderService;

@Configuration
@EnableScheduling
@ConditionalOnProperty(value = ConstantsHelper.SCHEDULER_WORKITEM_AUTOMATOR_ENABLED, havingValue = "true")
public class WorkItemAutomatorConfiguration {
   private static final Logger logger = LoggerFactory.getLogger(WorkItemAutomatorConfiguration.class);

   @Autowired
   ImapReaderService mail;

   @Scheduled(fixedDelayString = ConstantsHelper.SCHEDULER_WORKITEM_AUTOMATOR_FIXED_DELAY, initialDelayString = ConstantsHelper.SCHEDULER_WORKITEM_AUTOMATOR_INITIAL_DELAY)
   public void workItemAutomatorWorker() {
      logger.info("#workitemautomator start at {}.", DatesHelper.now());

      mail.run();
   }
}
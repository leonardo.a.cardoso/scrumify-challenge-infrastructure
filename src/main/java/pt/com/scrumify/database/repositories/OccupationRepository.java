package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pt.com.scrumify.database.entities.Occupation;

public interface OccupationRepository extends JpaRepository<Occupation, Integer> {
   @Query(nativeQuery = false,
          value = "SELECT o "+
                  "FROM Occupation o " +
                  "WHERE o.id > 1")
   List<Occupation> getAll();
}
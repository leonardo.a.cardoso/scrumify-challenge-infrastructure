package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_MENUS)
public class Menu implements Serializable {
   private static final long serialVersionUID = -6596888933854867087L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "name", length = 25, nullable = false)
   private String name;

   @Getter
   @Setter
   @Column(name = "href", length = 30, nullable = true)
   private String href;

   @Getter
   @Setter
   @Column(name = "css", length = 20, nullable = false)
   private String css;

   @Getter
   @Setter
   @Column(name = "icon", length = 25, nullable = true)
   private String icon;

   @Getter
   @Setter
   @Column(name = "submenu", nullable = true)
   private Boolean subMenu;

   @Getter
   @Setter
   @Column(name = "sortorder", nullable = false)
   private Integer order;

   @Getter
   @Setter
   @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
   @JoinTable(name = ConstantsHelper.DATABASE_TABLE_MENUS_ROLES, 
              joinColumns = @JoinColumn(name = "menu", referencedColumnName = "id", nullable = false, updatable = false), 
              inverseJoinColumns = @JoinColumn(name = "role", referencedColumnName = "id", nullable = false, updatable = false))
   private List<Role> roles;
}
package pt.com.scrumify.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.ApplicationLink;
import pt.com.scrumify.database.services.ApplicationLinkService;
import pt.com.scrumify.entities.ApplicationLinkView;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class ApplicationLinksController {
   @Autowired
   private ApplicationLinkService applicationsLinksService;

   /*
    * CREATE LINK IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_LINK_CREATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT)
   public String create(Model model, @PathVariable int app, @PathVariable int environment) {
      ApplicationLinkView view = new ApplicationLinkView();
      view.getApplication().setId(app);
      view.getEnvironment().setId(environment);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_LINK, view);

      return ConstantsHelper.VIEW_APPLICATIONS_LINK;
   }

   /*
    * UPDATE LINK IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_LINK_UPDATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT + ConstantsHelper.MAPPING_PARAMETER_LINK)
   public String update(Model model, @PathVariable int app, @PathVariable int environment, @PathVariable int link) {
      ApplicationLink applicationLink = this.applicationsLinksService.getOne(link, app, environment);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_LINK, new ApplicationLinkView(applicationLink));

      return ConstantsHelper.VIEW_APPLICATIONS_LINK;
   }

   /*
    * SAVE LINK IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #view.application.id)")
   @PostMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_LINK_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_LINK) @Valid ApplicationLinkView view) {
      this.applicationsLinksService.save(view.translate());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_LINKS, this.applicationsLinksService.getByApplicationAndEnvironment(view.getApplication().getId(), view.getEnvironment().getId()));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_LINKS;
   }

   /*
    * DELETE LINK IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_LINK_DELETE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT + ConstantsHelper.MAPPING_PARAMETER_LINK)
   public String delete(Model model, @PathVariable int app, @PathVariable int environment, @PathVariable int link) {
      this.applicationsLinksService.delete(new ApplicationLink(link));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_LINKS, this.applicationsLinksService.getByApplicationAndEnvironment(app, environment));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_LINKS;
   }
}